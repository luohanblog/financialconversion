### financialconversion
```financialconversion```适用于需要阿拉伯数字与中文数字之间互相转换的场景。
#### financialconversion特点
> - 以字符串的方式转换，没有超大数及浮点数等问题(请自行对原数据进行四舍五入等操作)
> - 支持口语化，如“壹佰亿零壹拾壹万零壹佰贰拾点贰壹”
> - 支持自定义转换（不论是兆还是厘都可以用）
> - 当然，你还可以把中文数字再转回阿拉伯数字
> - 支持自定义字符，随意转换

**注意：小数点后不超过4位，最大不要超过千兆，9999999999999999**
#### 安装
 ```node
 npm install financialconversion --save
 ```
#### 引用
```js
import num2str from 'financialconversion';
```
#### 示例
```js
// 返回 壹分
const money1 = num2str.numToStrMoney(0.01) 
// 返回 壹佰亿零壹拾壹万零壹佰贰拾圆贰角壹分
const money2 = num2str.numToStrMoney(10000110120.21) 
// 返回 10000110120.21
const money3 = num2str.strToNumMoney('壹佰亿零壹拾壹万零壹佰贰拾圆贰角壹分')
// 返回 负壹分
const money4 = num2str.numToStrMoney(-0.01) 

// 返回 零点零零肆
const num1= num2str.numToStr(0.004) 
// 返回 壹佰亿零壹拾壹万零壹佰贰拾点贰壹
const num2= num2str.numToStr(10000110120.21) 
// 返回 10000110120.21
const num3= num2str.strToNum('壹佰亿零壹拾壹万零壹佰贰拾点贰壹')
```

#### 修改语言
```js
num2str.setLanguage('cn')
// 返回 一百二十三圆整
const money1 = num2str.numToStrMoney(123)
num2str.setLanguage('hk')
// 返回 肆佰伍拾陸圓壹角貳分
const money2 = num2str.numToStrMoney(456.12)
```

#### 不止于此
**我们还可以根据自己的需求设置不同的字符串**
##### 例如
```js
// 将默认数字“0”，“1”分别改成了“凌”，“一”
num2str.setNums({ 0: "凌", 1: "一" })
// 将默认金额单位“圆”改成了“哈哈”
num2str.setMoneyUnits({1:'哈哈'})

// 返回 一佰亿凌一拾一万凌一佰贰拾哈哈贰角一分
const money1 = num2str.numToStrMoney(10000110120.21) 
// 返回 10000110120.21
const money2 = num2str.strToNumMoney('一佰亿凌一拾一万凌一佰贰拾哈哈贰角一分')

// 将默认小数分割符“点”别改成了“典”
num2str.setDecUnit({'.':'典'})
// 将默认位数“拾”，“佰”，“仟”分别改成了“时”，“摆”，“签”
num2str.setUnit({10: "时", 100: "摆", 1000: "签"})
// 返回 一摆亿凌一时一万凌一摆贰时典贰一
const num = num2str.numToStr(10000110120.21) 
```
#### API
|API| 参数 |默认参数|介绍
|--|--|--|--|
| setLanguage| language{ String}|"default"|可选："cn","hk"|
| setNums | nums{ Object  }||设置数字 详情见下 （获取参数示例）|
| setUnit | units{ Object  }||设置位数 详情见下 （获取参数示例）|
| setDecUnit| decUnit{ Object }||设置小数点或整 详情见下 （获取参数示例）|
| setMoneyUnits| moneyUnits{ Object }||设置金额单位 详情见下 （获取参数示例）|
| numToStr| num{ number or string }||数字转中文
| numToStrMoney| num{ number or string }||数字金额转中文金额
| strToNum| num{ string }||中文转数字
| strToNumMoney| num{ string }||中文金额转数字金额

#### 获取参数示例
```js
// 返回所有参数示例
num2str.getDefaultParams()
```
```js
{
  nums: {
    '0': '零',
    '1': '壹',
    '2': '贰',
    '3': '叁',
    '4': '肆',
    '5': '伍',
    '6': '陆',
    '7': '柒',
    '8': '捌',
    '9': '玖'
  },
  units: {
    '10': '拾',
    '100': '佰',
    '1000': '仟',
    '10000': '万',
    '100000000': '亿',
    '1000000000000': '兆'
  },
  decUnit: { 
    '.': '点', 
    '/': '整' 
  },
  moneyUnits: { 
    '1': '圆', 
    '0.1': '角', 
    '0.01': '分', 
    '0.001': '毫', 
    '0.0001': '厘' 
  }
}
```
#### 版本日志
>##### 1.1.4
> - **新增** 负数转换
> - **修复** 小数点后口语化问题
>##### 1.1.3
> - **新增** setLanguage('hk')转换繁体大写 @param{'default'|'cn'|'hk'}
> - **新增** 简体小写中文模板，“一，二，三，四”等，可以使用setLanguage('cn')切换
> - **修复** numToStrMoney(0)转换错误的问题
>##### 1.1.2
> - **修复** (0,1)区间内转换错误的问题
>##### 1.1.1
> - **优化** 设置自定义字符时可只设置单个字符
>##### 1.1.0
> - **新增** setNums实现自定义转换数字字符
> - **新增** setUnit实现自定义位数字符
> - **新增** setDecUnit实现自定义小数分割字符
> - **新增** setMoneyUnits实现自定义金额单位字符
> - **新增** getDefaultParams实现获取当前默认字符配置
> - **新增** numToStr实现数字转中文
> - **新增** numToStrMoney实现数字转中文金额
> - **新增** strToNum实现中文数字转阿拉伯数字
> - **新增** strToNumMoney实现中文数字金额转阿拉伯数字
> - **弃用** lowerToupper，upperTolower方法
>##### 1.0.1 已废弃
> - **修复** 转换大小限制，支持传入中文
> - **优化** 压缩代码体积，优化性能
>##### 1.0.0 已废弃
> - **新增** lowerToupper，upperTolower方法，实现大小写转换
