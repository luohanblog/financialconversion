import defaults from './default';
import cn from './cn';
import hk from './hk';

export default {
  defaults,
  cn,
  hk
}