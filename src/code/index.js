/*
 * @Date: 2023-12-02 20:43:40
 * @LastEditors: 骆
 * @LastEditTime: 2023-12-03 15:11:01
 * @FilePath: /financialconversion/src/code/index.js
 */
import libs from './libs/index';

/**
 * @function: 数字转中文
 * @param {*} num 数字
 * @param {*} defaultParams 转换字符
 * @param {*} isMoney 是否是金额
 * @return {string} 
 */
function num2strHandler(num, defaultParams, isMoney) {
  try {
    let strNum = num.toString()
    let is_f = strNum.includes('-')
    strNum = strNum.replace('-','')
    if (!strNum && strNum != '0') return ''
    let integer = strNum.split('.')[0]
    let decimal = strNum.split('.')[1] || ''
    // 整数部分
    // 去掉0开头的数字
    integer = integer.replace(/^0+/, '')
    if (integer.length > 4) {
      // 将整数部分按4位用,分割
      while (!/^\d{1,4}(,)/.test(integer)) {
        integer = integer.replace(/(\d)(\d{4}(\b|,))/, '$1,$2')
      }
    }

    let integerArr = integer.split(',').reverse()
    // 金额单位
    let unitArrReg = Object.values(defaultParams.units)
    let keyArr = Object.keys(defaultParams.units).sort((a, b) => b - a)
    let integerStr = ''

    integerArr.forEach((item, index) => {
      // 获取位数对应的单位
      let unitKey = Math.pow(10000, index)
      let unit = defaultParams.units[unitKey] || ''
      if (unitKey > 10000 && unit == '') {
        // 没有配置则取最大的遍历
        keyArr.forEach(key => {
          while (unitKey >= key) {
            unit = defaultParams.units[key] + unit
            unitKey = unitKey / key
          }
        })
      }
      // 设置千位单位
      let len = item.length;
      for (let i = 1; i < len; i++) {
        let z = len - i
        let unitKey = Math.pow(10, i)
        let unit = defaultParams.units[unitKey]
        let reg = new RegExp(`(\\d{${z}})([\\s\\S]*)`)
        item = item.replace(reg, `$1${unit}$2`).replace(/0$/, '')
      }
      let reg = new RegExp(`0([${unitArrReg.join('|')}])`, 'g')
      item = item.replace(reg, '0').replace(/0+/g, '0').replace(reg, '$1').replace(/[0]+$/, '')
      if (item != '') {
        item = item + unit
      }
      // 替换掉多余的0

      integerStr = item + integerStr
    })
    // 小数部分
    let decimalLen = decimal.length
    // 如果小数全是0，则置空
    let regDec = new RegExp(`0{${decimalLen}}`)
    decimal = decimal.replace(regDec, '')
    // 如果有小数并且不是金额，则加 点
    if (decimal != '' && !isMoney) decimal = defaultParams.decUnit['.'] + decimal

    if (isMoney) {
      if (decimal != '') {
        // 金额小数 设置单位
        for (let i = 0; i <= decimalLen; i++) {
          let z = decimalLen - i
          let moneyUnitKey = 1 / Math.pow(10, z)
          let moneyUnit = defaultParams.moneyUnits[moneyUnitKey] || ''
          let reg = new RegExp(`(\\d{${z}})([\\s\\S]*)`)
          decimal = decimal.replace(reg, `$1${moneyUnit}$2`)
        }
      } else {
        // 没有小数 则加尾缀
        decimal = defaultParams.moneyUnits['1'] + defaultParams.decUnit['/']
      }
      // 替换掉多余的0
      let moneyUnitArrReg = Object.values(defaultParams.moneyUnits)
      let decReg = new RegExp(`0(${moneyUnitArrReg.join('|')})`, 'g')
      decimal = decimal.replace(decReg, '0').replace(/0+/g, '0')
    }
    if (decimal != '' && integerStr == '') integerStr = '0'
    // 将数字转为大写
    let res = integerStr + decimal
    for (let i = 0; i < 10; i++) {
      let regZero = new RegExp(`^[0]+[${defaultParams.moneyUnits[1]}]`)
      let regZero2 = new RegExp(`^([\\s\\S]+)[0](${defaultParams.moneyUnits[1]})`)
      let regZero3 = new RegExp(`^([\\s\\S]+)[0](${defaultParams.decUnit['.']})`)
      let reg = new RegExp(`${i}`, 'g')
      let num = defaultParams.nums[i]
      res = res.replace(regZero, '').replace(regZero2, '$1$2').replace(regZero3, '$1$2')
      if (isMoney) {
        res = res.replace(/^[0]+/, '')
      }
      res = res.replace(reg, num)
    }
    if(is_f){
      res = defaultParams.decUnit['-'] + res
    }
    return res
  } catch (error) {
    console.error('请检查传入数字是否正确');;
  }
}

/**
 * @function: 中文转数字
 * @param {*} num 中文数字
 * @param {*} defaultParams 转换字符
 * @param {*} isMoney 是否是金额
 * @return {string} 
 */
function str2NumHandler(str, defaultParams, isMoney) {
  try {
    if (!str || str == '') return ''
    let is_f  = str.includes(defaultParams.decUnit['-'])
    str = str.replace(defaultParams.decUnit['-'],'')
    if (isMoney && !str.includes(defaultParams.moneyUnits['1'])) {
      str = '0' + defaultParams.moneyUnits['1'] + str
    }
    str = str.replace(defaultParams.moneyUnits['1'] + defaultParams.decUnit['/'], '').replace(defaultParams.moneyUnits['1'], '.').replace(defaultParams.decUnit['.'], '.')
    for (let i = 0; i < 10; i++) {
      let key = defaultParams.nums[i]
      let numReg = new RegExp(`${key}`, 'g')
      str = str.replace(numReg, i)
    }

    let myParams = {}
    Object.keys(defaultParams).forEach(childKey => {
      let obj = {}
      Object.keys(defaultParams[childKey]).forEach(key => {
        obj[defaultParams[childKey][key]] = key
      })
      myParams[childKey] = obj
    })
    let integer = str.split('.')[0]
    let decimal = str.split('.')[1] || ''
    let integerArr = integer.split('')
    let intUnitArr = []
    let _arr = []
    integerArr.forEach((item, index) => {
      let unit = myParams.units[item] || 'false'
      if (unit != 'false' && unit >= 10000) {
        let is_w = unit == 10000
        if (!is_w) {
          while (unit / 10000 >= 10000) {
            unit = unit / 10000
            is_w = unit == 10000
            if (index == integerArr.length - 1) {
              if (_arr.length == 0) _arr = [0, 0, 0, 0]
              intUnitArr.push(_arr)
              _arr = []
            }

          }
        }
        if (is_w) {
          if (_arr.length == 0) _arr = [0, 0, 0, 0]
          intUnitArr.push(_arr)
          if (index == integerArr.length - 1) {
            _arr = [0, 0, 0, 0]
          } else {
            _arr = []
          }
        }
      } else {
        _arr.push(item)
      }
    })
    if (_arr.length) intUnitArr.push(_arr)
    let resIntarr = []
    intUnitArr.map(item => {
      let newItem = [0, 0, 0, 0]
      let preNum = 0
      let _index = 0

      if (item.length == 1) {
        newItem[3] = item[0]
      } else if (item.length > 1) {
        item.reduce((pre, cur, index) => {
          if (/\d/.test(pre) && /[^\d]/.test(cur)) {
            let unit = myParams.units[cur].toString().replace(/[1]/, '')
            let _n = 3 - unit.length
            newItem[_n] = pre
            if ((preNum == cur || preNum == pre) && index - 1 == _index) {
              preNum = 0
              _index = 0
            }
          }
          if (/\d/.test(cur) && /[^\d]/.test(pre)) {
            preNum = cur
            _index = index
          }
          if (/\d/.test(cur) && /[\d]/.test(pre)) {
            preNum = cur
            _index = index
          }
          return cur
        })
      }

      if (preNum != 0) {
        newItem[3] = preNum
        preNum = 0
      }
      if (item.length) {
        resIntarr.push(newItem.join(''))
      }
    })
    let initStr = resIntarr.join('').replace(/^[0]+/, '')
    if (decimal != '' && initStr == '') {
      initStr = '0'
    }
    let res = initStr
    if (!isMoney && decimal != '') {
      res = initStr + '.' + decimal
    }
    if (isMoney && decimal != '') {
      decimal = decimal.replace(/[0]+/g, '')
      let decimalarr = decimal.split('')
      let decimalStr = ''
      let preLen = 0
      decimalarr.reduce((pre, cur) => {
        if (/\d/.test(pre) && /[^\d]/.test(cur)) {
          let unit = myParams.moneyUnits[cur].toString().replace(/0[.]/, '')
          let zeroNum = unit.length - preLen
          preLen = unit.length
          decimalStr += pre.toString().padStart(zeroNum, '0')
        }
        return cur
      })
      if (decimalStr != '' && initStr != '') {
        res = initStr + '.' + decimalStr
      } else {
        res = initStr + decimalStr
      }
    }
    if(is_f){
      res = '-' + res
    }
    return res
  } catch (error) {
    console.error('请检查传入文本是否正确或传入了自定义单位');
  }
}

class Num2Str {
  defaultParams = libs.defaults
  constructor() { }
  /**
   * @function: 设置转换语言
   * @param {'default'|'cn'|'hk'} language 
   * @return {void}
   */
  setLanguage(language) {
    switch (language) {
      case "cn":
        this.defaultParams = libs.cn
        break;
      case "hk":
        this.defaultParams = libs.hk
        break;
      default:
        this.defaultParams = libs.defaults
        break;
    }
  }
  /**
   * @function: 设置对应数字转换字符
   * @param {object} nums
   * @return {void}
   */
  setNums(nums) {
    if (typeof nums != "object" || Array.isArray(nums)) {
      console.error("setNums:请传入对象形式的参数")
      return
    }
    let num = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    let check = Object.keys(nums).every(item => {
      return num.includes(parseFloat(item))
    })
    if (!check) {
      console.error("setNums:传入参数不正确")
      return
    }
    this.defaultParams.nums = Object.assign(this.defaultParams.nums, nums)
  }
  /**
   * @function: 设置位数对应字符
   * @param {object} units
   * @return {void}
   */
  setUnit(units) {
    try {
      if (typeof units != "object" || Array.isArray(units)) {
        console.error("setUnit:请传入对象形式的参数")
        return
      }
      this.defaultParams.units = Object.assign(this.defaultParams.units, units)
    } catch (error) {

    }
  }
  /**
   * @function: 设置小数分割字符
   * @param {object} decUnit
   * @return {void}
   */
  setDecUnit(decUnit) {
    if (typeof decUnit != "object" || Array.isArray(decUnit)) {
      console.error("setDecUnit:请传入对象形式的参数")
      return
    }
    let num = ['.', '/', '-']
    let check = Object.keys(decUnit).every(item => {
      return num.includes(item)
    })
    if (!check) {
      console.error("setDecUnit:传入参数不正确")
      return
    }
    this.defaultParams.decUnit = Object.assign(this.defaultParams.decUnit, decUnit)
  }
  /**
   * @function: 设置金额单位字符
   * @param {object} moneyUnits
   * @return {void}
   */
  setMoneyUnits(moneyUnits) {
    if (typeof moneyUnits != "object" || Array.isArray(moneyUnits)) {
      console.error("setMoneyUnits:请传入对象形式的参数")
      return
    }
    this.defaultParams.moneyUnits = Object.assign(this.defaultParams.moneyUnits, moneyUnits)
  }
  /**
   * @function: 获取当前字符配置
   * @return {object}
   */
  getDefaultParams() {
    return this.defaultParams
  }
  /**
   * @function: 数字转中文
   * @param {string|number} num
   * @return {string}
   */
  numToStr(num) {
    return num2strHandler(num, this.defaultParams, false)
  }
  /**
   * @function: 数字转中文金额
   * @param {string|number} num
   * @return {string}
   */
  numToStrMoney(num) {
    return num2strHandler(num, this.defaultParams, true)
  }
  /**
   * @function: 中文数字转阿拉伯数字
   * @param {string} str
   * @return {string}
   */
  strToNum(str) {
    return str2NumHandler(str, this.defaultParams, false)
  }
  /**
   * @function: 中文数字金额转阿拉伯数字
   * @param {string} str
   * @return {string}
   */
  strToNumMoney(str) {
    return str2NumHandler(str, this.defaultParams, true)
  }
}

/**
 * @function: 数字转换
 * @example
 * // 返回 壹万零壹拾
 * num2str.numToStr(10010)
 * // 返回 10010
 * num2str.strToNum("壹万零壹拾")
 * // 返回 壹万零壹拾圆整
 * num2str.numToStrMoney(10010)
 * // 返回 10010
 * num2str.strToNum("壹万零壹拾圆整")
 * // 设置对应转换字符
 * num2str.setNums({1:'一'})
 * // 设置对应数字转换字符
 * num2str.setNums({1:'一'})
 * // 设置对应位数转换字符
 * num2str.setUnit({10:'十'，100:'百'})
 * // 设置对应小数分割字符
 * num2str.setDecUnit({'.':'丶'，'/':'。'})
 * // 设置对应金额单位
 * num2str.moneyUnits({1:"元",0.1:"角"})
 * // 获取默认字符
 * num2str.getDefaultParams()
 */
let num2str = new Num2Str()

export default num2str