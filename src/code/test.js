import num2str from './index'
num2str.setUnit({'1000000000000':'兆'})
console.log(num2str.numToStr(110.0001));
console.log(num2str.numToStr(10.0010));
console.log(num2str.numToStr(10.0100));
console.log(num2str.numToStr(0.1000));
console.log(num2str.numToStr(10.0001));
console.log(num2str.numToStr(10.0010));
console.log(num2str.numToStr('1000000000000'));
console.log(num2str.numToStrMoney('-10000110120.21'));
console.log(num2str.numToStrMoney('100.0101'));
console.log(num2str.numToStrMoney('10001.0101'));
console.log(num2str.numToStrMoney('1000100.0101'));
console.log(num2str.strToNum('壹佰壹拾点零零零壹'));
console.log(num2str.strToNum('壹拾点零零壹'));
console.log(num2str.strToNum('壹拾点零壹'));
console.log(num2str.strToNum('零点壹'));
console.log(num2str.strToNum('壹拾点零零零壹'));
console.log(num2str.strToNum('壹拾点零零壹'));
// console.log(num2str.strToNum('壹万亿'));
// console.log(num2str.strToNumMoney('壹佰亿零壹拾壹万零壹佰贰拾圆贰角壹分'));
// console.log(num2str.strToNumMoney('壹佰圆零壹分零壹厘'));
// console.log(num2str.strToNumMoney('壹万零壹圆零壹分零壹厘'));
// console.log(num2str.strToNumMoney('壹佰万零壹佰圆零壹分零壹厘'));


